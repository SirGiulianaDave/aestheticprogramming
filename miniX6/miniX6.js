// Defining variables
let img;
let ori;
let platform;
let x = 25;
let y = 250;
let speed = 5;
let jump = false;
let platforms = [];

//preloading my character
function preload(){
  img = loadImage('ori.png');
}

// setting up my canvas as well as my Objects
function setup(){
  createCanvas(1000,500);
  frameRate(30);
  ori = new Ori(25,250,2,2);
  platforms[0] = new Platform(200,400,100,30);
  platforms[1] = new Platform(350,300,100,30);
  platforms[2] = new Platform(200,200,100,30);
  platforms[3] = new Platform(500,100,100,30);
}



function draw(){
  background(51,172,233);

//here I tell the draw which functions from my objects it should draw
  ori.show();
  ori.move();
  ori.update();

  platforms[0].show();
  platforms[1].show();
  platforms[2].show();
  platforms[3].show();

//the 'goal' that doesn't work :((
  fill('yellow');
  ellipse(800,100,50,50);

  /*
  if (platforms[0].hits(ori)){
    console.log('HITS');
  }
    ^ first part of my attempt to make the platforms solid so you could land on them
      but it ended up with my defeat :((  */

  }
  function keyPressed (){
    if (key == ' '){
      ori.up();
    }
  }


class Ori{
  constructor(x,y){
    this.x = x;
    this.y = y;

// by setting up a fake gravity i make sure that my character always fall down (see update())
    this.gravity = 1;
    this.velocity = 0;
  }

// this is to make the character move left and right :))
  move(){
    if(keyIsDown(RIGHT_ARROW)){
      this.x += speed;
    }
    if(keyIsDown(LEFT_ARROW)){
      this.x -= speed;
    }
  }

// here I counteract the gravity to simulate a "jump" :)
  up(){
    this.velocity += -this.gravity*17;
    print(this.velocity);
  }

// here the simulated gravity is determined
  update(){
    this.velocity += this.gravity;
    this.y += this.velocity;

// when the character hits the bottom of the screen velocity stops, so you have a solid ground
    if (this.y > height) {
      this.y = height;
      this.velocity = 0;
}
}

// what is shown on screen - my chracter
  show(){
    imageMode(CENTER);
    image(img, this.x, this.y-22, 50, 50);
}


}

class Platform{
  constructor(x,y,w,h){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
}
/*
    hits(ori){
      if (ori.y < this.y){
      }
    }
  ^ my sad attempt at trying to make make the platforms a solid object that you can land on :((
  this went through many different renditions but these remnants are just here to showcase my defeat
*/

// the platforms
  show(){
    fill(255,255,255);
    rect(this.x,this.y,this.w,this.h);
  }
}
