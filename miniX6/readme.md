# miniX6 - Jeppe Lund (WORK IN PROGRESS)
### Image of the game
![](screen.png)
### Links
[program](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX6/)

[code](https://gitlab.com/SirGiulianaDave/aestheticprogramming/-/blob/main/miniX6/miniX6.js)
## The game
This week I unfortunately coulnd't get the program to work :/ I'm really pleased that i managed to set up a very basic physics system, so that whenever a setup character jumped, it'd fall down due to a simulated gravity (super simple, just two lines of code). Essentially I wanted my game to be a super simple platformer and I had most of the elements set up, but I unfortunately couldn't figure out a way to apply some physics to some platforms so that my character could stay on the platform without falling through it. I set up an object to be my main character, one of my favorite video game characters Ori, as well as the platforms to hop on. These objects all have values assigned to them, such as x,y,w,h and velocity/gravity which are used to have them appear/function in a certain way. I see the point of making of making it objects rather than just uploading images and symbols and changing their x & y values, as you can have the objects relate them to one another, but I just couldn't figure out how to make it work ://

(SPOILER ALERT: I MADE IT WORK (see miniX7))

## Conceptually
Video Games are something I hold very dear, so I find it very natural to think about objects as a vital part of digital culture. Video Games are mostly object oriented, in the sense that most things in a game interact with one another. Your character interacts with everything around them, and those thing might interact with other things as well, so I feel as a video game enjoyer it might be easier to see the "patterns" of object oriented programming, at least within the subject of gaming. I find object oriented programming really interesting, because I like the way you, as the developer, are able to have your code "work together" as well as be more efficient. 

I've really enjoyed working on this project, cus I feel that the process I went through to make it work, although, simple still is something that resembles what I wanna do in life... at least so far :))

### Sources:
- [Ori sprite](http://pixelartmaker.com/art/cfbe17483bc2608)
