# miniX3 - Jeppe Lund

## Instructions:
![](capture.gif)

* hold your cursor over the throbber to make it go counter-clockwise :))

### Links to the program and code:

[Program](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX3/)

[Code](https://gitlab.com/SirGiulianaDave/aestheticprogramming/-/blob/main/miniX3/miniX3.js)

## The idea behind it
I wanted to do something that looked like a video game loading screen, but I wasn't sure what I wanted to do. Since its launch I've been going stupid on Elden Ring, so I guess my mind has been stuck in The Lands Between, so I think I wanted to incorporate something within that sort of vibe. I wanted some swords to spin, kind of like a loading symbol, so that's what I wanted to do. I also liked the idea of it being able to be affected by the user, so that when you touch it, it starts spinning the other way, so that the user starts questioning whether the loading process is being halted. Essentially, I just wanted to make something that gives the impression that time is passing and that something is happening and a turning circle gives that impression in my opinion (see watches, wheels, planets etc.(I know it's a globe, but you get the gist of it ;))))) I'm not sure if I accomplished the task regarding loops, but I would argue that the draw function is in itself a loop, soooo yay loophole :)



## How does it work?
I first took one sword and put it instead of the circle in Winnie's code (spinning load icon), but it looked like poop, so I just took a screenshot of the static result (the 8 swords) and plonked them into photoshop, where I used the magic eraser to fuck off the background. I then took this image and had it spin around itself, by translating the origin to be in the middle of the middle of the screen and had the x and y values of the image be 0.

I also made a conditional sentence that makes it so when the mouse cursor is within the x and y value of of the swords, then spinning goes goes the other way.

See the code for better understanding :))

Very simple program, without anything to say really ://



Thx for reading have nice day :))) <3




## Sources:
[Sword](https://www.picng.com/down/82211)

Special thanks - Jeremie Simon Zacharie Vincent
