//first i define my image (swords) and the angle used for the rotation later
let swords;
let angle = 0;


function setup(){
  createCanvas(800,800);
  //took this from the references when looking for rotate(). Changes it the mode from the default of RADIANS
  angleMode(DEGREES);
}
//preload my one image.
function preload(){
swords = loadImage('assets/swords.png')
}

function draw(){
  background(0);

  push();
  //for it to spin around itself i needed the x and y values of my image to be 0, so i had to use translate to put in in the middle of the screen :))
  translate(400,400)
  rotate(angle);
  imageMode(CENTER);
  image(swords,0,0)
  pop();
//when it draws, it sets the angle to 1, which makes it rotate clockwise.
  angle = angle + 1

//if you hold your mouse over the spinning swords it will decrease the angle by 2, making it turn counter-clockwise.
//if the value was -1 it would just stop the spinning.
  if (mouseX > 235 && mouseX < 560 && mouseY > 240 && mouseY < 560){
    angle = angle - 2
  }
  fill(255,50,255);
  textSize(40)
  textAlign(CENTER,CENTER)
  textStyle(BOLD);
  text('REVERT THE PROGRESS',400,100);
  text('REVERT THE PROGRESS',400,700);
  }


/*
  let num = 8
  push()
  frameRate(5);
  translate(windowWidth/2+100,windowHeight/2+50);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  scale(0.05);
  imageMode(CENTER);
  image(sword,1700,0)
  pop()

  ^^
  this whole thing is what i initially wanted to use to make the swords spin, but i really didn't like the look of it
  so once i had the swords in a circle, i just plonked it into photoshop and mate it into an image that i could just rotate around itself,
  thus making it smoother :)) figured i'd keep it in here :))
*/
