# Non-Reversible Allow feat. Cheeky Sunglasses 
![](Capture1.png)
![](Capture2.png)
![](Capture3.png)

## Links
[RunMe](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX4/)

[Code](https://gitlab.com/SirGiulianaDave/aestheticprogramming/-/blob/main/miniX4/miniX4.js)

## Description
When dealing with the phrase "CAPTURE ALL", I wasn't entirely sure as to what I wanted to do, but I defnitely wanted to something with facial capture, because it's something that intrigues me and that i know very little about. My program is mostly based on the code that we worked on in [week 5](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class05), but i tweaked it a little to fit what i wanted to do. 

Essentially my program is your camera capturing your face via. the face capturing code and face model we were introduced to in class and  then masking your face with a black ellipse. Underneath the black ellipse I made some very basic sunglasses that tracks the center of your eyes, and thus positioning themselves correctly (for the most part:)) I then through DOM, created two buttons; one that lets you remove the face-covering ellipse and the other lets you remove the sunglasses. The interesting thing I did is then that the sunglasses are able to return (once you remove the mouse from the button), while the ellipse will never come back, unless you of course refresh the program. 
I guess I did this in an attept to symbolize how, once we allow programs to use our data, it can sometimes be hard or almost impossible to go back, while the pointless funny thing (the sunglasses) can be removed and put back on as you please. Sooo i guess the point of the program is to showcase how the capture of data can be hard to have any control over, if at all. 

It's a very basic program, I know, but some of my other ideas were too advanced for me to accomplish. One of them was trying to track the edges of your lips and try to recognize when you're smiling, but i just found it too advanced to do for now, but it's defnitely something i can see myself working on later :))

## Reflection on data capturing
Data capture plays a huge part in our society, whether it be facial recognition used in law enforcement or it be gathering your search history in order to give you targeted ads online. Whenever I hear anything about data capture, it's always about something terrible or controversial like how black people are disproportionately getting arrested based on faulty facial recognition or how some big coorporation is stealing our data. 

Without knowing too much about the subject, I defnitely feel that it's something we as people should be more focused on because things like data rights, facial recognition and AI are only going to become a bigger issue in the future.


Nothing crazy in this week's project, but thank you for reading :))

Have a lovely week <3
