//Defining things
let button;
let ctracker;
let capture;
let val = 255;
let val1 = 255;

function setup(){
  createCanvas(640,480);
  button = createButton('Allow face to be shown');
  button.style("xxx","xxx"); //e.g button.style("color", "#fff");
  button.size(150,50);
  button.position(0,0);
  button.mousePressed(change);

  button1 = createButton('Remove glasses');
  button1.style("xxx","xxx"); //e.g button.style("color", "#fff");
  button1.size(150,50);
  button1.position(0,0);
  button1.mousePressed(change1);
  button1.mouseOut(revertStyle1);
  noStroke();


  //web cam capture
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

}


function draw() {

  //draw the captured video on a screen

  image(capture, 0,0, 640, 480);



  let positions = ctracker.getCurrentPosition();
 //check the availability of web cam tracking
 if (positions.length) {
    //point 60 is the mouth area
   button.position(width/2-250, 400);
   button1.position(width/2+100, 400);
   fill(0,0,0,val1);
   push();
   stroke(0,0,0,val1);
   strokeWeight(10);
   line(positions[64][0], positions[64][1], positions[68][0], positions[68][1]);
   line(positions[23][0], positions[23][1], positions[0][0], positions[0][1]);
   line(positions[28][0], positions[28][1], positions[14][0], positions[14][1]);
   pop();
   ellipse(positions[27][0], positions[27][1], 70, 50);
   ellipse(positions[32][0], positions[32][1], 70, 50);

   push();
   fill(0,0,0,val);
   ellipseMode(CENTER);
   ellipse(positions[62][0], positions[62][1], 250, 300);
   pop();
 }


}

function change() {
  val = 0;
}

function change1() {
  val1 = 0;
}

function revertStyle1() {
  val1 = 255;
}
