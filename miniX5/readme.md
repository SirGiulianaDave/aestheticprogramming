# miniX5 - Jeppe Lund
### First image shows the first frame of the program:
![](1.png)
### Second image shows the program when it's "done":
![](2.png)
## Links
[Program](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX5/)

[Code](https://gitlab.com/SirGiulianaDave/aestheticprogramming/-/blob/main/miniX5/miniX5.js)

## Description
This week's miniX is fairly simple :) Essentially i just put an ellipse within a for loop with at all its values being randomized (to a degree). Every time a new ellipse is made its colour, size and position is randomized. After a while the whole screen is going to be filled up with circles of different shapes and colours, gererated (mostly) at random to make sure one execution of the program is never identical to another. There's a timer set up to make the program stop after a certain amount of time, the intention behind this being that I wanted you to have a "product" as the outcome. I know this kind of ruins the point of it running forever on its own, so to kind of mend this I also added some optional controls not mentioned in the program itself

**"s" lets you stop the program whenever you want to**

**"a" lets you continue the program, even if it has stopped due to the timer**

In terms of rules I guess i primarily have two, one being that the x and y values have to be within the screen of the viewer, and the other being that the colour and the size of the ellipses have to be random. Without these "rules" my program would simply be an ellipse doing jack sh*t.

Conceptually I struggled to come up with an idea for what to do with this theme of automatic generation, as I generally like my programs to "have a point" if you know what I mean. Just randomly having the program plonking stuff on screen isn't really satisfying to me, though i know i could've of course just made a more interesting program. Initially I wanted to make a randomly generated story, but I simply didn't have the time to write sufficiently intrigueing plots, so that idea just got scrapped xD


## Reflection on Auto-Generation
I don't really know how to feel about generated art and content. For me, one of the great aspects of art is how its elements are intended to be in a certain way by the artist, so once you start tinkering with "intention" and instead auto generates some of the elements, I start losing interest. Things like those hideous NFT monkeys are absolutely terrible cases of this in my opinion. They look truly hideos and pointless, and the fact that it's random might have you get a monkey that looks truly nonsensical with no thematic conformity. Just to clarify I really know much about the subject and thus don't know how exactly these monkeys (and other nft's) are generated, but yeah, it just doesn't really speak to me. 

On the other hand, some aspects of generated art really cool to me, especially when you think of game development. Lets say you want to add a mountain range to your game, but you don't want to sit their and model every single aspect of each individual mountain/hill. It situations like this you can model a few different variations of mountains into a machine learning program, and then have the 3d software (fx Unity) create auto generated mountains. This of course is possible for many other things, such as grass textures and walls etc. I think this is super cool because it reduces the amount of work required to create something huge, giving the devs more time to focus on more aspects of the game. This, as mentioned earlier, does take something away from the 3d sculpting artists as their job of hand molding objects is made less important, as well as possibly making the generated objects look more generic as it no longer has that "human touch". 

In my opinion generated art can be both bad and beneficial, it just really depends on where you utilize it. Things that need a certain level of attention to detail should be hand crafted, whereas more "background" things could be generated to reduce workload. 

Thanks for reading and have a lovely week <3 :)
