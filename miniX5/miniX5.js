// defining a timer that i use later
let t = 0;

function setup(){
  //sets up the canvas to fit the viewers screen, as well as framerate and background colour
  createCanvas(windowWidth,windowHeight);
  frameRate(15);
  background(0);
}

function draw(){
// Defining the x and y values to be the viewer's screen size, so that when i use the random command the program knows to not go out of bounds
  let x = windowWidth
  let y = windowHeight

  for(let i = 0; i < 10; i++){
    // for-loop determines the spawn-rate of the ellipses
    fill(random(255),random(255),random(255))
    ellipse(random(x),random(y),random(200),random(200));
  }

// here is set up a timer, so that the program freezes after a certain amount of time
  t++
  if (t == 200){
    // there is probably a better way, but setting the frameRate to 0 makes sense to me xD
    frameRate(0);
  }
}

// This function is there to let the user start/stop the program as they please.
//83 = s & 65 = a
function keyPressed() {
  if (keyCode === 83) {
    frameRate(0);
  } else if (keyCode === 65){
    frameRate(30);
  }


}
// just here to ensure that the program always fits the window :))
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

// Thanks for reading :))
