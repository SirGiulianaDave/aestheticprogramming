//DISCLAIMER: the bools are only used as a debugging tool
//Enjoy!

//defining the mp3 names
let polo;
let eldenring;
let kpop;

//defining the images
let caucasian;
let african_american;
let asian;

//defining the boolean values
let bool1 = false;
let bool2 = false;
let bool3 = false;

//firstly i setup my canvas to fit your the viewer's screen
function setup(){
  createCanvas(windowWidth,windowHeight);
}

//here i prelaod all the music and images required later
function preload(){
//first i preload the mp3 files
  polo = createAudio('assets/polo.mp3');
  eldenring = createAudio('assets/eldenring.mp3');
  kpop = createAudio('assets/kpop.mp3')

//then i preload the image files for the emojis
  caucasian = loadImage('assets/caucasian.png');
  african_american = loadImage('assets/african_american.png');
  girl = loadImage('assets/girl.png')
}

function draw(){
  background(0);

//insert caucasian emoji
  imageMode(CENTER);
  image(caucasian,150,300)

//insert african_american emoji (here i had to use push and pop commands to scale this specific image)
  imageMode(CENTER);
  push();
  scale(0.5);
  image(african_american,1280,600);
  pop();

//insert girl emoji (Here i also used push and pop)
  imageMode(CENTER);
  push()
  scale(1.6)
  image(girl,707,180);
  pop()

//insert text
  fill(255,255,255);
  textAlign(CENTER);
  textSize(45);
  text('WHAT ARE THE INDIVIDUALS LISTENING TO?',650,100);

  fill(255,255,255);
  textAlign(CENTER);
  textSize(45);
  text('IS IT WHAT YOU EXPECTED?',650,600);

//here i determine the frame in which the music needs to play
//kpop
  if (mouseX > 50 && mouseX < 250 && mouseY > 200 && mouseY < 400){
    bool1 = true
    kpop.volume(0.2);
    kpop.play();
}
    else {
    kpop.stop();
    }

//Elden Ring
  if (mouseX > 540 && mouseX < 740 && mouseY > 200 && mouseY < 400){
      bool2 = true
      eldenring.volume(0.3);
      eldenring.play();
  }
    else {
      eldenring.stop();
    }

//Polo G
  if (mouseX > 1030 && mouseX < 1230 && mouseY > 200 && mouseY < 400){
        bool3 = true
        polo.volume(0.2);
        polo.play();
    }
    else {
        polo.stop();
    }
}
