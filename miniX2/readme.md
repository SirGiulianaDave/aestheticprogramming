# miniX2 - Jeppe Lund

## Instructions:
![](Capture.PNG)

1. imagine what the people might listen to
2. Hover the mouse over an emoji and music should play
3. (if nothing's playing click on the screen once and it should fix it :))

### Links to the program and code:

[Program](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX2/)

[Code](https://gitlab.com/SirGiulianaDave/aestheticprogramming/-/blob/main/miniX2/miniX2.js)

### Idea behind it
When we were given the assignment i couldn't really think of anything to do, all i knew was just that i wanted to do somehing with loading music into the program. Once i'd thought a little about what music to pop in, i got the idea to play music that would stereotypically fit a certain emojis. 

I came up with 3 songs of different genres: Rap: Polo G, K-Pop: TWICE, Orchestral: Yuka Kitamura, as well as three people they would fit to. Stereotypically a black man might listen to rap, an old white man might listen to Orchestral/Classical and a teenage girl might listen to K-pop. In my program it's mixed up tho, so that the white man is listening to kpop, the black man listens to orchestral and the girl listens to rap music.

I guess point is that we sometimes have certain stereotypes of what kind of music certain people listen to, while in reality everyone listens to all kinds of different stuff. 

That's basically it, thanks for reading ;)

EXTRA:
Obviously my own stereo typical view has an influence on what music they listen to, so i would've liked to randomize the songs so that whenever you hover the mouse over an emoji a random song would play, rather than one i've predetermined, but yeah i didn't really know how to do that soooooo... that's for another time :))

### Sources

Polo G - Black man in America

Yuka Kitamura - Elden Ring Main Theme

TWICE - Cheer up!
