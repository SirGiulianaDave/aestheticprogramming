// Defining variables
let img;
let victory;
let naru;
let ori;
let x = 25;
let y = 250;
let speed = 5;
let platforms = [];

//preloading my character and music
function preload(){
  img = loadImage('assets/ori.png');
  victory = createAudio('assets/nibel.mp3');
  naru = createAudio('assets/naru.mp3');
}

// setting up my canvas as well as my Objects
function setup(){
  createCanvas(1000,500);
  frameRate(30);
  ori = new Ori(25,250,2,2);
  platforms[0] = new Platform(200,400,100,30);
  platforms[1] = new Platform(350,300,100,30);
  platforms[2] = new Platform(500,200,100,30);
  platforms[3] = new Platform(650,250,100,30);


}



function draw(){
  background(51,172,233);
  naru.volume(0.2);
  naru.play();
//here I tell the draw which functions from my objects it should draw
  ori.show();
  ori.move();
  ori.update();

  if (platforms[0].hits(ori)){
    ori.y = ori.y;
    ori.velocity = -1;
  }
  if (platforms[1].hits(ori)){
    ori.y = ori.y;
    ori.velocity = -1;
  }
  if (platforms[2].hits(ori)){
    ori.y = ori.y;
    ori.velocity = -1;
  }
  if (platforms[3].hits(ori)){
    ori.y = ori.y;
    ori.velocity = -1;
  }



  platforms[0].show();
  platforms[1].show();
  platforms[2].show();
  platforms[3].show();




//the 'goal' that doesn't work :((
  fill('yellow');
  ellipse(800,100,50,50);

  textAlign(LEFT);
  textSize(20);
  text('JUMP IS BROKEN, SO PLS ONLY JUMP ONCE :)',25,150);
  text('SPACE = JUMP',25,100);
  text('ARROW KEYS TO MOVE',25,50);

  if (ori.x > 770 && ori.x < 830 && ori.y > 75 && ori.y < 125){
    console.log('nice');
    background(0,200,0);
    textAlign(CENTER);
    textSize(100);
    text('LEVEL COMPLETE',width/2,height/2);
    naru.stop();
    victory.volume(0.2);
    victory.play();
    noLoop();

  }



  }
  function keyPressed (){
    if (key == ' '){
      ori.up();
    }
  }


class Ori{
  constructor(x,y,w,h){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

// by setting up a fake gravity i make sure that my character always fall down (see update())
    this.gravity = 1;
    this.velocity = 0;
  }

// this is to make the character move left and right :))
  move(){
    if(keyIsDown(RIGHT_ARROW)){
      this.x += speed;
    }
    if(keyIsDown(LEFT_ARROW)){
      this.x -= speed;
    }
  }

// here I counteract the gravity to simulate a "jump" :)
  up(){
    this.velocity += -this.gravity*17;
  }

// here the simulated gravity is determined
  update(){
    this.velocity += this.gravity;
    this.y += this.velocity;

// when the character hits the bottom of the screen velocity stops, so you have a solid ground
    if (this.y > height) {
      this.y = height;
      this.velocity = 0;
}
}

// what is shown on screen - my chracter
  show(){
    imageMode(CENTER);
    image(img, this.x, this.y-22, 50, 50);
}


}

class Platform{
  constructor(x,y,w,h){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
}

    hits(ori){
      if (ori.x >= this.x-this.w/2 && ori.x <= this.x + this.w/2 && ori.y + ori.h >= this.y - this.h/2 && ori.y+ori.h <= this.y + this.h/2){
        return true;
      }
      return false;
    }


// the platforms
  show(){
    fill(255,255,255);
    rectMode(CENTER);
    rect(this.x,this.y,this.w,this.h);
  }
}
