# J.P. Lund miniX7 | Fix up week

### when the game launches:
![](screen.png)
### when you beat the game
![](screen2.png)
## TO-DO LIST:
**The things I Have to clear up for this week's miniX**
- Make sure the cryptpad is all green (miniX6 readme too short) -> DONE
- Finish/fix my miniX6 -> DONE
- Make readme for this miniX (what I changed and why) -> DONE

### Links:
[Program](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX7/)

[Code](https://gitlab.com/SirGiulianaDave/aestheticprogramming/-/blob/main/miniX7/miniX7.js)

[Earlier "broken" version](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX6/)
### Thing I changed/added to my miniX7
- Added a (somewhat) working collision system to my program !!!
- Made the game beatable (rearranged the platforms slightly as well as adding a finish)
- Added some dope ass music to the game (Ori OST)

### Reasoning behind those changes/addtions
I chose to go back to this particular miniX for a few reasons, the main ones being that I didn't manage to finish it the first time around, as well as it being the project I've been most interested in this semester, due to it being in the world of Video Games. I struggled figuring out how to make collision work (and i still do), but in classic programming fashion I found i way, by searching around the internet for methods of doing so. Essentially it's just maths of saying if the x and y values of of my game character lies within the x and y values of the platforms, I then tell the program to counteract the velocity of the character going downwards (making it stop on the platform). On a conceptual level I knew that this was a way to do it, but until this week I was unable to comprehend the logical math aspect of it (because I'm a fool xD). It still doesn't work perfectly, so it just goes to show that even when you break past one barrier, another one appears, which just motivates one further :))

The working platformers are vital for obvious reasons, but I also felt the game needed some great music to accompany it, as well as a way of "beating" it, for it to feel like  you we're actually playing a level in game. To be a bit critical, I wish could've sorted the collision issues earlier, so I could've had a go at trying to make an actual hard level, as well as maybe multiple levels and also a limited double jump button, but yea for now it's still just a model for a game, with only the very basics implemented. I like how I came into this week thinking I was going to fix my "game", but instead it just showed me more things that need fixing, which is just motivating me to learn more :)

As a lover of video games I really wanted to make something that actually worked, so I was really frustrated last week when I didn't manage exactly that, so being able to come back at later time with questions and more energy in the tank was super fun. I think this project more than any of the earlier ones made me realize that I really need to work within a field I have genuine interest in, as this naturally makes the try to make something great, even if that means failing over and over again. I think this is a common way of working when it comes to aesthetic programming. Sometimes you find the logical solution straight away, but other times you have to find it through trial and error, which can be very frustrating, but also one of the most satisfying aspects of it all :))

I've fixed the yellows in the cryptpad, so it should all be clear and now that I've reworked this little platformer, I'm exciting to start the the bigger collaborative projects :))


### Sources:
- [Ori sprite](http://pixelartmaker.com/art/cfbe17483bc2608)
- 'Light of Nibel' by Gareth Coker
- 'Naru, Embracing the Light' by Gareth Coker

