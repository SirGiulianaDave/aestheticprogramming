## First readme
Unsure as to what to write here, I guess I will just tell you about what I've done for the first miniX1. Considering my limited ability in programming as well as my lacking creativity I knew my idea had to be something simple and easily executed. I had just tried out the game Football Manager a few days earlier which gave me the inspiration to make something football themed.

I wanted to take the simplest play in football, a penalty, a make it into a little game. Calling what I've made a game is perhaps a bit of a stretch, but it reacts to a button input so hey :) Bellow is a picture of what you see when you run my code:

![](Capture.PNG)

You're the penalty taker and by pressing either the left or right arrow you take a shot. For now all that happens when you press either button is just that the ball teleports to a specified location. No animation or simulated physics. I tried figuring out how to animate the ball "flying" into the net as well as maybe having the goalkeeper attempt to save it, but I couldn't find a way with my limited ability. I also wanted the user to be able to reset the code by pressing a key, because in the current state you have to manually reload the page to take another shot, but I guess it's just one of the many things to learn in the future.

### How it works and the basics
[Link to the program](https://sirgiulianadave.gitlab.io/aestheticprogramming/miniX1/)

[Link to the code](https://gitlab.com/SirGiulianaDave/aestheticprogramming/-/blob/main/miniX1/miniX1.js)

Essentially the program is just some preloaded images that appear or move when certain buttons are clicked. See the code for explanations of what specifically is happening, although it is so incredibly basic there's really no need :)

Feedback and ideas are greatly appreciated! :)

Have a nice day and thanks for reading <3
