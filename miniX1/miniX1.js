//Here i define the x and y values of where the ball should be placed, so it can be changed later
let x = 300;
let y = 500;

//here i just setup the basic canvas
function setup() {
createCanvas(600,600);
background(50);
}

//images from a local folder are preloaded into the code so they can be user later
function preload() {
goal = loadImage('goal.png')
ball = loadImage('ball.png')
keeper = loadImage('goalkeeper.png')
fans = loadImage('fans.png')
}

//this section contains all the things you see when you boot the code (images.etc)
function draw() {
fill(23,235,87);
rect(0,300,600,300);
imageMode(CENTER);
image(goal,300,200);
imageMode(CENTER);
image(ball,x,y,50,50)
imageMode(CENTER);
image(keeper,300,280,200,200);
fill(255,0,0);
textAlign(CENTER);
text('PRESS EITHER LEFT OR RIGHT ARROW',300,570);
textAlign(CENTER);
text('RELOAD PAGE BETWEEN EACH SHOT',300,590);
}

//the keyPressed function is what determines where the ball moves once you press a certain key
function keyPressed() {
if (keyCode === RIGHT_ARROW){
  y = 300
  x = 500
  image(fans,300,150,600,300)
}
if (keyCode === LEFT_ARROW){
  y = 300
  x = 100
  image(fans,300,150,600,300)
}
//Since i coulnd't find a way to animate the ball, the keypress just changed the x and y values
}
