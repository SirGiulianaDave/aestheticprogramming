// defining variables
let names;
let qandA;

let qc;

let button1;
let button2;
let button3;
let button4;

let images = [];


function preload(){ // preloading the necessary files
  names = loadJSON('names.json');
  qandA = loadJSON('questions.json');

  images[0] = loadImage('assets/Squirtle.png');
  images[1] = loadImage('assets/Charizard.png');
  images[2] = loadImage('assets/Pikachu.png');
  images[3] = loadImage('assets/Snorlax.png');
  images[4] = loadImage('assets/Vulpix.png');
  images[5] = loadImage('assets/Psyduck.png');
  images[6] = loadImage('assets/Lickitung.png');
  images[7] = loadImage('assets/Chansey.png');
  images[8] = loadImage('assets/Magikarp.png');
  images[9] = loadImage('assets/Kadabra.png');
  images[10] = loadImage('assets/MrMime.png');
  images[11] = loadImage('assets/Slowpoke.png');
  song = loadSound('assets/pokemonsong.mp3')
  backdrop = loadImage('assets/backdrop.png')
}

function setup(){
createCanvas(windowWidth,windowHeight)
background(backdrop)
song.play()
qc=0
buttons()
}

function changeValue1(){ // the changeValue functions determine what happens when you click the buttons
  for(let i=0;i<=11;i++){
  names.pokemon[i].points=names.pokemon[i].points+qandA.questions[qc].p1[i]; // determines how many points a specific pokemon should get, depending on answer
  }
    qc++;
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();

    if(qc == 6){
      finish()
    }else{
      buttons();
    }
}

function changeValue2(){
  for(let i=0;i<=11;i++){
  names.pokemon[i].points=names.pokemon[i].points+qandA.questions[qc].p2[i];
  }
    qc++;
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();

    if(qc == 6){
      finish()
    }else{
      buttons();
    }
}

function changeValue3(){
  for(let i=0;i<=11;i++){
  names.pokemon[i].points=names.pokemon[i].points+qandA.questions[qc].p3[i];
  }
    qc++
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();

    if(qc == 6){
      finish()
    }else{
      buttons();
    }
}

function changeValue4(){
  for(let i=0;i<=11;i++){
  names.pokemon[i].points=names.pokemon[i].points+qandA.questions[qc].p4[i];
  }

    qc++;
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();

    if(qc == 6){ // when the qc amount reaches 6 the function finish() is drawn
      finish()
    }else{
      buttons();
    }

}


function mousePressed(){ // purely used for making console.log()
  console.log('Squirtle',names.pokemon[0].points);
  console.log('Charizard',names.pokemon[1].points);
  console.log('Pikachu',names.pokemon[2].points);
  console.log('Snorlax',names.pokemon[3].points);
  console.log('Vulpix',names.pokemon[4].points);
  console.log('Psyduck',names.pokemon[5].points);
  console.log('Lickitung',names.pokemon[6].points);
  console.log('Chansey',names.pokemon[7].points);
  console.log('Magikarp',names.pokemon[8].points);
  console.log('Kadabra',names.pokemon[9].points);
  console.log('Mr. Mime',names.pokemon[10].points);
  console.log('Slowpoke',names.pokemon[11].points);
  console.log(Math.max.apply(Math, names.pokemon.map(function(o) { return o.points; })));
}

function buttons(){ // here the buttons are made
  push()
  background(backdrop)
  textStyle(BOLD);
  textSize(35);
  textAlign(CENTER);
  text(qandA.questions[qc].q,width/2,height/2); // the questions determined by json


  button1 = createButton(qandA.questions[qc].a[0]) //answer 1 determined by json
  button1.position(width/8,height/2+50);
  button1.mousePressed(changeValue1);
  button1.size(200, 100);

  button2 = createButton(qandA.questions[qc].a[1]); //answer 2 determined by json
  button2.position(width/2.95,height/2+50);
  button2.mousePressed(changeValue2);
  button2.size(200, 100);

  button3 = createButton(qandA.questions[qc].a[2]); //answer 3 determined by json
  button3.position(width/1.815,height/2+50);
  button3.mousePressed(changeValue3);
  button3.size(200, 100);

  button4 = createButton(qandA.questions[qc].a[3]); //answer 4 determined by json
  button4.position(width/1.31,height/2+50);
  button4.mousePressed(changeValue4);
  button4.size(200, 100);
  pop()
  }

function finish(){ // the screen you see when you've answered all the questions
  var res = Math.max.apply(Math,names.pokemon.map(function(o){return o.points;}))
  var obj = names.pokemon.find(function(o){ return o.points == res; })
  // the two variables above determine which pokemon got the most points
  console.log(obj.name)
  console.log(names.pokemon.indexOf(obj))

  background(backdrop);
  fill(255,255,255);
  textAlign(CENTER);
  textStyle(BOLD);
  textSize(60);
  text('Congratulations! Your Pokemon is '+obj.name+'!',width/2,height/2-250); // obj.name makes sure that it's the correct name from the json that's displayed

  imageMode(CENTER);
  image(images[names.pokemon.indexOf(obj)],width/2,height/2,400,400); // makes sure the picture shown at the end is always the pokemon with most points
}
